package com.example.test1.network

import com.example.test1.BuildConfig
import kotlinx.coroutines.runBlocking
import okhttp3.*
import java.io.IOException
import javax.inject.Inject

class AuthenticationInterceptorRefreshToken @Inject constructor(
    private val retrofitBuilder: RetrofitBuilder,
    private val userManager: TokenModel
) : Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response? {
        //MAKE SYNCHRONIZED
        synchronized(this) {
            val originalRequest = chain.request()
            val authenticationRequest = originalRequest.newBuilder()
                .addHeader("Authorization", "Bearer ${userManager.accessToken}")
                .addHeader("appVersion", BuildConfig.VERSION_NAME).build()
            val initialResponse = chain.proceed(authenticationRequest)

            when {
                initialResponse.code() == 403 || initialResponse.code() == 401 -> {
                    initialResponse.close()
                    //RUN BLOCKING!!
                    val responseNewTokenLoginModel = runBlocking {
                        retrofitBuilder.retrofitAuth().create(ApiRefreshToken::class.java)
                            .refreshToken(userManager.refreshToken).execute()
                    }

                    return when {
                        responseNewTokenLoginModel.code() != 200 -> {
                            // Go to login
                            null
                        }
                        else -> {
                            responseNewTokenLoginModel.body()?.accessToken?.let {
                                userManager.accessToken = it
                            }
                            val newAuthenticationRequest = originalRequest.newBuilder().addHeader(
                                "Authorization",
                                "Bearer " + responseNewTokenLoginModel.body()?.accessToken
                            ).build()
                            chain.proceed(newAuthenticationRequest)
                        }
                    }
                }
                else -> return initialResponse
            }
        }
    }
}

private fun request(originalRequest: Request): Request {
    return originalRequest.newBuilder()
        .addHeader("Authorization", "Bearer ${userManager.load()?.accessToken}")
        .addHeader("appVersion", BuildConfig.VERSION_NAME).build()
}


