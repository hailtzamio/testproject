package com.example.test1.network

import android.os.UserManager
import com.example.test1.BuildConfig
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitBuilder {

    fun retrofitAuth(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(createClientAuth())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun createClientAuth(): OkHttpClient {
        //ADD DISPATCHER WITH MAX REQUEST TO 1
        val dispatcher = Dispatcher()
        dispatcher.maxRequests = 1
        val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
        okHttpClientBuilder.dispatcher(dispatcher)
        okHttpClientBuilder.addInterceptor(AuthenticationInterceptorRefreshToken(this, TokenModel()))
        return okHttpClientBuilder.build()
    }
}